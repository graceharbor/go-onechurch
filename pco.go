package pco

import "net/http"

// NewConnection returns a PCO Connection in which the module versions used
// are determined by the token configuration (i.e. the X-PCO-API-Version header
// is unset)
func NewConnection(appId, token string) *Connection {
	return NewVersionedConnection(appId, token, nil)
}

// NewVersionedConnection returns a PCO Connection that uses a specific version
// for each module, specified by the `versions` mapping.  Unspecified module
// versions fallback to the token configuration.
func NewVersionedConnection(appId, token string, versions map[ModuleName]string) *Connection {
	return &Connection{
		auth:     &tokenAuth{appId, token},
		versions: versions,
		client:   http.DefaultClient,
		root:     apiRoot,
	}
}

// NewBearerConnection returns a PCO Connection using an OAuth token.
func NewBearerConnection(token string) *Connection {
	return &Connection{
		auth:   &bearerAuth{token},
		client: http.DefaultClient,
		root:   apiRoot,
	}
}
