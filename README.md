Planning Center Golang API
==========================

This is a work-in-progress golang package for providing access to the
[Planning Center](https://www.planningcenter.com/)
[API](https://developer.planning.center/docs/#/overview/).

Tests are run against the real PCO API if the env vars are set:
 - `PCO_TEST_APP` set to your app ID
 - `PCO_TEST_TOKEN` set to the associated token
else, the tests are skipped.
