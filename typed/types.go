package pco_typed

import (
	"encoding/json"
	"time"

	"gitlab.com/graceharbor/go-pco"
)

var defaultVersions = map[pco.ModuleName]string{
	pco.People: "2023-02-15",
}

func NewConnection(appId, token string) *pco.Connection {
	return pco.NewVersionedConnection(appId, token, defaultVersions)
}

type Email struct {
	pco.Identifier
	Address   string
	IsBlocked bool      `json:"blocked"`
	Created   time.Time `json:"created_at"`
	Updated   time.Time `json:"updated_at"`
	Location  string
	IsPrimary bool `json:"primary"`
}
type Phone struct {
	pco.Identifier
	Number string
	// National  *string   `json:"national"`
	Created   time.Time `json:"created_at"`
	Updated   time.Time `json:"updated_at"`
	Location  string
	IsPrimary bool `json:"primary"`
}

type Date time.Time

func (d *Date) UnmarshalJSON(input []byte) (err error) {
	var str string
	err = json.Unmarshal(input, &str)
	if err != nil {
		return err
	}
	date, err := time.Parse("2006-01-02", str)
	if err != nil && str != "" {
		return
	}
	*d = Date(date)
	return nil
}
func (d *Date) Time() time.Time { return time.Time(*d) }

type Address struct {
	pco.Identifier
	City        string
	CountryCode string    `json:"country_code"`
	Country     string    `json:"country_name"`
	Created     time.Time `json:"created_at"`
	Location    string
	IsPrimary   bool `json:"primary"`
	State       string
	Street      string
	Updated     time.Time `json:"updated_at"`
	Zip         string
}

type Person struct {
	pco.Identifier
	Name        string // display name
	GivenName   string `json:"given_name"`
	FirstName   string `json:"first_name"`
	Nickname    string
	MiddleName  string `json:"middle_name"`
	FamilyName  string `json:"last_name"`
	Birthdate   Date
	Anniversary Date
	Grade       int
	IsChild     bool `json:"child"`
	Gender      string
	Membership  string
	Created     time.Time `json:"created_at"`
	Updated     time.Time `json:"updated_at"`
	Avatar      string

	// `json:"graduation_year"` // : 1,
	// `json:"site_administrator"` // : true,
	// `json:"accounting_administrator"` // : true,
	// `json:"people_permissions"` // : "string",
	// `json:"inactivated_at"` // : "2000-01-01T12:00:00Z",
	// `json:"status"` // : "string",
	// `json:"medical_notes"` // : "string",
	// `json:"mfa_configured"` // : true,
	// `json:"demographic_avatar_url"` // : "string",
	// `json:"directory_status"` // : "string",
	// `json:"can_create_forms"` // : true,
	// `json:"can_email_lists"` // : true,
	// `json:"passed_background_check"` // : true,
	// `json:"school_type"` // : "string",
	// `json:"remote_id"` // : 1,
	// `json:"directory_shared_info"` // : {}
}
