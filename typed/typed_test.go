package pco_typed_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/graceharbor/go-pco"
	pco_testing "gitlab.com/graceharbor/go-pco/internal/testing"
	pco_typed "gitlab.com/graceharbor/go-pco/typed"
)

func getConn(t *testing.T) *pco.Connection {
	appId, token := pco_testing.GetTestEnv(t)
	return pco_typed.NewConnection(appId, token)
}

func TestRelationships(t *testing.T) {
	r := require.New(t)
	c := getConn(t)
	r.NotNil(c)
	result, err := c.GetMultiplef(context.Background(), "people", "/people?include=%s&per_page=%d", "emails", 1)
	r.NoError(err)
	r.NotNil(result)
	r.NotZero(result.Data)
	for i := 0; i < 50; i++ {
		r.GreaterOrEqual(len(result.Data), 1)
		person := result.Data[0]
		var personData pco_typed.Person
		r.NoError(person.As(&personData))
		t.Logf("id:   %s\n", personData.ID)
		t.Logf("name: %s\n", personData.Name)
		r.Contains(person.Relationships, "emails")

		emails := person.Relationships["emails"]
		if len(emails) > 0 {
			var email pco_typed.Email
			r.NoError(emails[0].As(&email))
			r.Equal(email.Identifier, emails[0].Identifier)
			t.Logf("address: %s\n", email.Address)
			t.Logf("blocked: %v\n", email.IsBlocked)
			return
		}
	}
	t.Log("failed to find any emails")
	t.Fail()
}
