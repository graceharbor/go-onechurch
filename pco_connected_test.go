package pco_test

import (
	"context"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/graceharbor/go-pco"
	pco_testing "gitlab.com/graceharbor/go-pco/internal/testing"
)

func getConn(t *testing.T) *pco.Connection {
	appId, token := pco_testing.GetTestEnv(t)
	return pco.NewConnection(appId, token)
}

func TestSingle(t *testing.T) {
	r := require.New(t)
	c := getConn(t)
	r.NotNil(c)
	result, err := c.Get(context.Background(), "people", "/birthday_people")
	r.NoError(err)
	r.NotNil(result)
	r.NotZero(result.Datum.ID)
}

func TestMultiple(t *testing.T) {
	r := require.New(t)
	c := getConn(t)
	r.NotNil(c)
	result, err := c.GetMultiple(context.Background(), "people", "/people")
	r.NoError(err)
	r.NotNil(result)
	r.NotZero(result.Data)
	r.GreaterOrEqual(len(result.Data), 1)
	id, err := strconv.Atoi(result.Data[0].ID)
	r.NoError(err)
	r.GreaterOrEqual(id, 1)

	r.True(result.HasMore())
	result, err = result.Next()
	r.NoError(err)
	r.GreaterOrEqual(result.Meta.Count, 1)
}

func TestMultipleAll(t *testing.T) {
	r := require.New(t)
	c := getConn(t)
	r.NotNil(c)
	result := c.GetAllChan(context.Background(), "people", "/people")
	defer result.Close()
	select {
	case <-result.Done():
		t.Log(result.Err())
		t.FailNow()
	default:
	}
	r.GreaterOrEqual(result.TotalValues(), 1)
	first := <-result.Values
	r.Equal(first.Type, "Person")
	r.NotZero(first.ID)
	result.Close()
	<-result.Done()
	_, open := <-result.Values
	r.False(open)
}

func runManyPeople(t *testing.T, perPage int, minExpected int) {
	r := require.New(t)
	c := getConn(t)
	r.NotNil(c)
	result := c.GetAllChanf(context.Background(), "people", "/people?per_page=%d", perPage)
	defer result.Close()
	r.Greater(result.TotalValues(), minExpected)
	// get up to the rate limit...
	var i int
	for range result.Values {
		i++
		if i > minExpected {
			break
		}
	}
	t.Logf("ran %d API calls", c.TotalCallCount())
}

func TestMany(t *testing.T) {
	runManyPeople(t, 20, 41)
}
