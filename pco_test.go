package pco

import (
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"path/filepath"
	"testing"
)

type testPCOServer struct {
	TestDataSubdir string
	t              *testing.T
	server         *httptest.Server
}

// Setup creates and initializes the test fixture
func (tp *testPCOServer) setup() {
	testDataDir := path.Join("testdata", tp.TestDataSubdir)
	// Create a new test HTTP server
	tp.server = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		path := filepath.Join(testDataDir, r.URL.Path)
		stat, err := os.Stat(path)
		switch {
		case err == nil && stat.IsDir():
			path = filepath.Join(path, "index.json")
		case err != nil:
			path += ".json"
		}
		stat, err = os.Stat(path)
		if err != nil || stat.IsDir() {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		data, err := os.ReadFile(path)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		switch filepath.Ext(path) {
		case ".json":
			w.Header().Set("Content-Type", "application/json")
		}
		w.WriteHeader(http.StatusOK)
		w.Write(data)
	}))
}

// Teardown stops the test HTTP server
func (tp *testPCOServer) teardown() {
	tp.server.Close()
}

func (tp *testPCOServer) GetURL() string {
	return tp.server.URL
}

func getTestServer(t *testing.T, dirname string) (*testPCOServer, *http.Client) {
	tp := &testPCOServer{dirname, t, nil}
	tp.setup()
	return tp, tp.server.Client()
}

type testPCOConnection struct {
	*Connection
	server *testPCOServer
}

func (tc *testPCOConnection) setup() {
}
func (tc *testPCOConnection) Close() error {
	tc.teardown()
	return nil
}

// Teardown stops the test HTTP server
func (tc *testPCOConnection) teardown() {
	tc.server.teardown()
}

func GetTestConnection(t *testing.T, dirname string) *testPCOConnection {
	tp, client := getTestServer(t, dirname)
	tp.setup()
	tc := &testPCOConnection{
		server: tp,
		Connection: &Connection{
			auth:     &tokenAuth{"", ""},
			versions: nil,
			client:   client,
			root:     tp.GetURL(),
		},
	}
	tc.setup()
	return tc
}
