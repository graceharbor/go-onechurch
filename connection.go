package pco

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	"github.com/pkg/errors"
)

const (
	apiRoot        = "https://api.planningcenteronline.com"
	retryAfter     = "Retry-After"
	versionKey     = "X-PCO-API-Version"
	pcoVersion     = "v2"
	rateLimitError = http.StatusTooManyRequests
	retryAfterKey  = "Retry-After"
)

// ModuleName is a string representing a given API module (the first part of
// an API endpoint path)
type ModuleName string

const (
	People     ModuleName = "people"
	API        ModuleName = "api"
	Calendar   ModuleName = "calendar"
	CheckIns   ModuleName = "check-ins"
	Giving     ModuleName = "giving"
	Groups     ModuleName = "groups"
	Publishing ModuleName = "publishing"
	Services   ModuleName = "services"
	Webhooks   ModuleName = "webhooks"
)

type authenticator interface {
	Set(req *http.Request)
}

type tokenAuth struct {
	app   string
	token string
}

func (t *tokenAuth) Set(req *http.Request) {
	req.SetBasicAuth(t.app, t.token)
}

type bearerAuth struct {
	token string
}

func (t *bearerAuth) Set(req *http.Request) {
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", t.token))
}

type Connection struct {
	auth  authenticator
	calls atomic.Int32

	root string

	versions map[ModuleName]string
	client   *http.Client
}

// SetAPIRoot overrides the default PCO API root
func (c *Connection) SetAPIRoot(root string) { c.root = root }

// TotalCallCount returns the total number of PCO API calls made by this connection
func (c *Connection) TotalCallCount() int { return int(c.calls.Load()) }

// GetRawf is a convenience wrapper to GetRaw, passing endpoint and values to fmt.Sprintf.
func (c *Connection) GetRawf(ctx context.Context, module ModuleName, endpoint string, values ...any) ([]byte, error) {
	return c.GetRaw(ctx, module, fmt.Sprintf(endpoint, values...))
}

// GetRaw gets the raw bytes from an api call
func (c *Connection) GetRaw(ctx context.Context, module ModuleName, endpoint string) ([]byte, error) {
	// URL to make the GET request to
	if !strings.HasPrefix(endpoint, "/") {
		endpoint = "/" + endpoint
	}
	endpoint = "/" + string(module) + "/" + pcoVersion + endpoint

	req, err := http.NewRequestWithContext(ctx, "GET", c.root+endpoint, nil)
	if err != nil {
		return nil, err
	}
	if c.versions != nil {
		moduleVersion, ok := c.versions[module]
		if ok {
			req.Header.Add(versionKey, moduleVersion)
		}
	}
	c.auth.Set(req)
	var waitSeconds = 0
	var triedOnce bool
	var response *http.Response
	for !triedOnce || waitSeconds > 0 {
		triedOnce = true
		if waitSeconds > 0 {
			select {
			case <-ctx.Done():
				return nil, ctx.Err()
			case <-time.After(time.Second * time.Duration(waitSeconds)):
			}
			waitSeconds = 0
		}
		response, err = c.client.Do(req)
		if err != nil {
			return nil, errors.Wrapf(err, "via PCO endpoint %s", endpoint)
		}
		c.calls.Add(1)
		if response.StatusCode == rateLimitError {
			waitSecondsStr := response.Header.Get(retryAfter)
			if waitSecondsStr != "" {
				waitSeconds, err = strconv.Atoi(waitSecondsStr)
				if err != nil {
					return nil, errors.Wrapf(err, "invalid %s value", retryAfterKey)
				}
			}
		} else if response.StatusCode < 200 || response.StatusCode >= 300 {
			data, _ := io.ReadAll(response.Body)
			return data, fmt.Errorf("%s via endpoint %s", response.Status, endpoint)
		}
	}

	defer response.Body.Close()
	return io.ReadAll(response.Body)
}

func (c *Connection) getDecoded(ctx context.Context, module ModuleName, endpoint string, result any) (err error) {
	data, err := c.GetRaw(ctx, module, endpoint)
	if err != nil {
		return
	}
	err = json.NewDecoder(bytes.NewReader(data)).Decode(result)
	return
}
