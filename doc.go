// package pco provides a golang interface to the v2 Planning Center API,
// automatically handling PCO's rate-limiting and response paging.
//
// The interface distinguishes between single datum responses
// (e.g. /people/v2/birthday_people) or responses with multiple data
// (e.g. /people/v2/people).  All versions of each module are handled
// automatically, so long as PCO continues to use the same format in its v2 API.
//
// Relationships and Includes are also supported, with relationship maps being
// automatically populated from the returned `included` list.
//
// Note that all returned data are in raw JSON form (json.RawMessage).  Each
// datum (a `BasicItem`) contains an `As` method for unmarshalling the contents
// into a golang struct.  See `gitlab.com/graceharbor/go-pco/typed` for an
// example implementation.
package pco
