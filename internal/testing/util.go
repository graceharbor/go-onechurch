package pco_testing

import (
	"os"
	"testing"
)

func GetTestEnv(t *testing.T) (appId, token string) {
	appId = os.Getenv("PCO_TEST_APP")
	token = os.Getenv("PCO_TEST_TOKEN")
	if appId == "" || token == "" {
		t.Skip("PCO test env not set")
	}
	return appId, token
}
