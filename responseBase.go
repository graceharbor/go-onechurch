package pco

import (
	"bytes"
	"encoding/json"

	"github.com/pkg/errors"
)

type Links struct {
	Self    *string
	Related *string
	Next    *string
}

type Identifier struct {
	Type string
	ID   string
}

func (i *Identifier) setID(id Identifier)       { i.Type, i.ID = id.Type, id.ID }
func (i *Identifier) GetIdentifier() Identifier { return Identifier{i.Type, i.ID} }

func (b *Item) GetAttributes() []byte { return b.Attributes }

type idSetter interface {
	setID(Identifier)
}

type BaseItemI interface {
	privateFunction()
	GetAttributes() []byte
	GetIdentifier() Identifier
}

func (b *Item) privateFunction() {}

func setID(value any, id Identifier) {
	setter, ok := value.(idSetter)
	if ok {
		setter.setID(id)
	}
}

// As unmarshals a BaseItem's attributes into `value`.  Be sure value is a
// reference or a pointer.
func (b *Item) As(value any) (err error) {
	err = json.Unmarshal(b.GetAttributes(), value)
	if err != nil {
		err = errors.Wrapf(err, "error extracting data for %s %s into %T", b.GetIdentifier().Type, b.GetIdentifier().ID, value)
		return
	}
	setID(value, b.GetIdentifier())
	return
}

type ItemRelationships map[string][]Item

func (ir *ItemRelationships) UnmarshalJSON(data []byte) (err error) {
	var relationshipMap = make(map[string]struct {
		Data json.RawMessage
	})
	err = json.Unmarshal(data, &relationshipMap)
	if err != nil {
		return
	}
	*ir = make(ItemRelationships, len(relationshipMap))
	for key, relation := range relationshipMap {
		var singleID Identifier
		var realData []Item
		switch {
		case bytes.Equal(relation.Data, []byte("null")):
		case json.Unmarshal(relation.Data, &singleID) == nil:
			realData = append(realData, Item{singleID, []byte{}, Links{nil, nil, nil}, nil})
		default:
			var multiID []Identifier
			err = json.Unmarshal(relation.Data, &multiID)
			if err != nil {
				return
			}
			for _, v := range multiID {
				realData = append(realData, Item{v, []byte{}, Links{nil, nil, nil}, nil})
			}
		}
		(*ir)[key] = realData
	}
	return
}

type Item struct {
	Identifier
	Attributes    json.RawMessage
	Links         Links
	Relationships ItemRelationships
}

// BaseResponse is the basic structure of a PCO v2 API response.  It is incomplete,
// as it does *not* contain the `data` field, which is either an object for
// responses with a single datum value, or a list of objects for responses with
// multiple data.
type BaseResponse struct {
	Links    Links
	Included []Item
	Meta     struct {
		Total  int `json:"total_count"`
		Count  int
		Parent Identifier
	}

	included map[Identifier]Item
}

// GetIncluded returns the BaseItem for a particular Type/ID pair from the
// list of included results.
func (b *BaseResponse) GetIncluded(id Identifier) (Item, bool) {
	if b.included == nil {
		b.included = make(map[Identifier]Item)
		for _, v := range b.Included {
			b.included[v.Identifier] = v
		}
	}
	ret, ok := b.included[id]
	return ret, ok
}

func getIncluded(value *Item, response *BaseResponse) {
	for _, relation := range value.Relationships {
		for i, value := range relation {
			newItem, ok := response.GetIncluded(value.Identifier)
			if ok {
				relation[i] = newItem
			}
		}
	}
}
