package pco

import (
	"context"
	"fmt"
)

type SingleResponse struct {
	BaseResponse
	Datum Item `json:"data"`
}

// Getf is a convenience wrapper to Get, passing endpoint and values to fmt.Sprintf.
func (c *Connection) Getf(ctx context.Context, module ModuleName, endpoint string, values ...any) (result *SingleResponse, err error) {
	return c.Get(ctx, module, fmt.Sprintf(endpoint, values...))
}

// Get returns the results of an endpoint with a single datum (it is an error
// to call this function on an endpoint that returns more than one object).
func (c *Connection) Get(ctx context.Context, module ModuleName, endpoint string) (result *SingleResponse, err error) {
	result = &SingleResponse{}
	err = c.getDecoded(ctx, module, endpoint, result)
	if err != nil {
		return
	}
	getIncluded(&result.Datum, &result.BaseResponse)
	return
}
