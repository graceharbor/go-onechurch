package pco_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/graceharbor/go-pco"
	pco_typed "gitlab.com/graceharbor/go-pco/typed"
)

// TestExample demonstrates usage of the test fixture
func TestQuickSingle(t *testing.T) {
	conn := pco.GetTestConnection(t, "server")
	defer conn.Close()

	res, err := conn.Get(context.Background(), pco.People, "/")
	require.NoError(t, err)
	require.Equal(t, res.Datum.ID, "1234")
}
func TestQuickMultiple(t *testing.T) {
	conn := pco.GetTestConnection(t, "server")
	defer conn.Close()
	res := conn.GetAllChan(context.Background(), pco.People, "/people")

	defer res.Close()
	people := make(map[string]string)
	for p := range res.Values {
		var person pco_typed.Person
		p.As(&person)
		people[p.ID] = person.Name
	}
	require.NoError(t, res.Close())
	require.NoError(t, res.Err())
	require.Len(t, people, 2)
	require.Contains(t, people, "1235")
	require.Contains(t, people, "1236")
	require.Equal(t, people["1235"], "Example Person")
	require.Equal(t, people["1236"], "Ex Person")
}
