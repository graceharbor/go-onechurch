package pco

import (
	"context"
	"fmt"
	"strings"
	"sync"
)

type MultipleResponse struct {
	BaseResponse
	Data []Item

	conn   *Connection
	ctx    context.Context
	module ModuleName
}

// HasMore returns true if there are more data to be paged.
func (p *MultipleResponse) HasMore() bool { return p.Links.Next != nil }

// Next returns a MultipleResponse object for the next page of data.
func (p *MultipleResponse) Next() (next *MultipleResponse, err error) {
	if !p.HasMore() {
		err = fmt.Errorf("no more results")
		return
	}
	url := *(p.Links.Next)
	if !strings.HasPrefix(url, p.conn.root) {
		err = fmt.Errorf("next url does not have PCO root prefix")
		return
	}
	url = strings.TrimPrefix(url, p.conn.root)
	url = strings.TrimPrefix(url, "/")
	if !strings.HasPrefix(url, string(p.module)) {
		err = fmt.Errorf("next url does not use the same module")
		return
	}
	url = strings.TrimPrefix(url, string(p.module))
	url = strings.TrimPrefix(url, "/")
	if !strings.HasPrefix(url, pcoVersion) {
		err = fmt.Errorf("expected next URL with version %s", pcoVersion)
		return
	}
	url = strings.TrimPrefix(url, pcoVersion)
	url = strings.TrimPrefix(url, "/")
	return p.conn.GetMultiple(p.ctx, p.module, url)
}

// GetMultiplef is a convenience wrapper to GetMultiple, passing endpoint and values to fmt.Sprintf.
func (c *Connection) GetMultiplef(ctx context.Context, module ModuleName, endpoint string, values ...any) (result *MultipleResponse, err error) {
	return c.GetMultiple(ctx, module, fmt.Sprintf(endpoint, values...))
}

// GetMultiple returns a MultipleResponse object, beginning the retrieval of
// paged results.
func (c *Connection) GetMultiple(ctx context.Context, module ModuleName, endpoint string) (result *MultipleResponse, err error) {
	result = &MultipleResponse{
		conn:   c,
		ctx:    ctx,
		module: module,
	}
	err = c.getDecoded(ctx, module, endpoint, result)
	if err != nil {
		return
	}
	for i := range result.Data {
		getIncluded(&result.Data[i], &result.BaseResponse)
	}
	return
}

type AllResponses struct {
	total  int
	ctx    context.Context
	Values <-chan Item
	cancel context.CancelFunc
}

// TotalValues returns the total number of data in all pages of this request.
func (p *AllResponses) TotalValues() int { return p.total }

// Close finishes this paged set of requests (closing the associated results channel).
func (p *AllResponses) Close() error {
	p.cancel()
	<-p.Done()
	return p.Err()
}

// Done returns a channel that is closed upon retrieving all requests.
func (p *AllResponses) Done() <-chan struct{} { return p.ctx.Done() }

// Err returns nil until Done is closed, then it returns an error that was
// encoutered by the request, if any.
func (p *AllResponses) Err() error {
	select {
	case <-p.Done():
	default:
		return nil
	}
	err := context.Cause(p.ctx)
	switch err {
	case context.Canceled:
		fallthrough
	case context.DeadlineExceeded:
		return nil
	default:
		return err
	}
}

// GetAllChanf is a convenience wrapper to GetAllChan, passing endpoint and values to fmt.Sprintf.
func (c *Connection) GetAllChanf(ctx context.Context, module ModuleName, endpoint string, values ...any) (result *AllResponses) {
	return c.GetAllChan(ctx, module, fmt.Sprintf(endpoint, values...))
}

// GetAllChan returns an AllResponses object, which provides access to all
// paged data in a response via a channel.  The user is responsible for
// calling Close on the result (which is an io.Closer).
func (c *Connection) GetAllChan(ctx context.Context, module ModuleName, endpoint string) (result *AllResponses) {
	result = &AllResponses{}
	var userCtx context.Context
	userCtx, result.cancel = context.WithCancel(ctx)
	var because context.CancelCauseFunc
	result.ctx, because = context.WithCancelCause(ctx)
	multiple, err := c.GetMultiple(ctx, module, endpoint)
	if err != nil {
		because(err)
		return
	}
	result.total = multiple.Meta.Total
	values := make(chan Item)
	result.Values = values

	var wg sync.WaitGroup

	go func() {
		var err error
		defer func() { because(err) }()
		defer wg.Wait()
		defer func() {
			for range values {
			}
		}()
		defer close(values)
		for multiple != nil {
			for _, v := range multiple.Data {
				select {
				case <-result.ctx.Done():
					return
				case <-userCtx.Done():
					err = fmt.Errorf("user cancelled")
					return
				case values <- v:
				}
			}
			if multiple.HasMore() {
				multiple, err = multiple.Next()
				if err != nil {
					return
				}
			} else {
				multiple = nil
			}
		}
	}()
	return
}
